extends Path2D

onready var follow = get_node("follow")
var tween

func _ready():
	set_process(true)

func _process(delta):
	follow.set_offset(follow.get_offset() + 200 * delta)
	
	
func _on_seen():
	set_process(false)
	
func _on_reset():
	set_process(true)

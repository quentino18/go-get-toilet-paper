extends KinematicBody2D

var speedmultiplier = 200
signal sprint
signal healthchange
signal moneychange
signal game_over
signal Interraction
var sprinting = false
var StaminaMax = 0
var InvicibilityFrame = false

onready var startpoint = Vector2(position.x,position.y)

export var Stamina : int setget Stamina_set, Stamina_get
export var Health : int setget Health_set, Health_get


func _ready():
	self.connect("sprint",self.get_tree().get_root().find_node("SprintBar",true,false),"_on_sprint_updated")
	self.connect("healthchange",self.get_tree().get_root().find_node("HealthBar",true,false),"_on_health_updated")
	self.connect("moneychange", self.get_tree().get_root().find_node("Money",true,false),"_on_money_loss")
	StaminaMax = self.get_tree().get_root().find_node("SprintBar",true,false).find_node("TextureProgress").max_value
	Stamina = self.get_tree().get_root().find_node("SprintBar",true,false).find_node("TextureProgress").value
	Health = self.get_tree().get_root().find_node("HealthBar",true,false).find_node("TextureProgress").value
	$BoostTime.connect("timeout",self,"_on_sprint_finished")
	$StaminaUpTimer.connect("timeout",self,"_get_stamina_up")

func _unhandled_input(event):
	if event.is_action("INTERRACT"):
		emit_signal("Interraction")
	
func _physics_process(_delta):
	var velocity = Vector2() 
	if(Input.is_action_pressed("LEFT")):
		var AxisValue = Input.get_action_strength("LEFT")
		velocity.x -=  1
	if(Input.is_action_pressed("RIGHT")):
		var AxisValue = Input.get_action_strength("RIGHT")
		velocity.x +=  1
	if(Input.is_action_pressed("UP")):
		var AxisValue = Input.get_action_strength("UP")
		velocity.y -=  1
	if(Input.is_action_pressed("DOWN")):
		var AxisValue = Input.get_action_strength("DOWN")
		velocity.y +=  1
	
	if(Input.is_action_pressed("SPRINT") && Stamina > 333 && !sprinting ):
		speedmultiplier = 400
		Stamina = Stamina - 333
		sprinting = true
		emit_signal("sprint", Stamina)
		$BoostTime.start()

	velocity = velocity.normalized() * speedmultiplier
	self.move_and_slide(velocity)
	
		#Annimation
	if velocity.length() > 0:
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()
		
	if velocity.x > 0:
		$AnimatedSprite.animation = "right"
	elif velocity.x < 0:
		$AnimatedSprite.animation = "left"
	elif velocity.y < 0:
		$AnimatedSprite.animation = "up"
	elif velocity.y > 0:
		$AnimatedSprite.animation = "down"
	else:
		$AnimatedSprite.animation = "idle"
		
	
func _get_stamina_up():
	if(Stamina + 5 <= StaminaMax):
		Stamina+=5
		emit_signal("sprint", Stamina)
		

		
	
func _on_sprint_finished():
	speedmultiplier = 200
	sprinting = false
	
func Stamina_set(v):
	Stamina = v
	
func Stamina_get():
	return Stamina

func Health_set(v):
	Health = v
	emit_signal("healthchange", Health)
	if Health <= 0:
		emit_signal("game_over","You got corona Virus")
	
	
func Health_get():
	return Health
	
func _on_Hit(damage):
	if InvicibilityFrame == false:
		Health_set(Health - damage)
		$InvicibilityFrame.start()
		InvicibilityFrame = true
	
func _on_contact(node):
	if node.type == "Policier" && InvicibilityFrame == false:
		emit_signal("moneychange",1)
		$InvicibilityFrame.start()
		InvicibilityFrame = true

func _on_InvicibilityFrame_timeout():
	InvicibilityFrame = false

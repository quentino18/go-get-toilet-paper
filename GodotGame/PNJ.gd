extends KinematicBody2D

var speed = 120
var path = PoolVector2Array() setget set_path
var collision
var velocity = Vector2()
var player_in_range
var player_in_sight
var count = 0
var animation
var linenode

#export var typeList: 
export var type : String setget set_type, get_type

#preloaded var
var AOE = preload("res://assets/PNJ/Attack/Contamination_Area.tscn").instance()
var sight = preload("res://assets/PNJ/Attack/Sight.tscn").instance()

#node and position at start of the game
onready var nav_2d : Navigation2D = self.get_tree().get_root().find_node("Navigation2D",true,false)
onready var player = get_tree().get_root().find_node("Player",true,false)
onready var initposition: Vector2 = position


signal playerseen()
signal playercontact(node)

func _ready():
	self.connect("playerseen",get_parent().get_parent(),"_on_seen")
	self.connect("posreset",get_parent().get_parent(),"_on_reset")
	self.connect("playercontact", player, "_on_contact")
	#load animation and damage abilities depending on the type of PNJ
	match type:
		"Policier":
			animation = load("res://assets/PNJ/Animations/AnimatedSprite_Cop.tscn").instance()
			self.add_child(animation)
			sight.connect("body_entered",self,"_on_Sight_body_entered")
			sight.connect("body_exited",self,"_on_Sight_body_exited")
			self.add_child(sight)
			pass
		"H malade":
			AOE.damage = 20
			self.add_child(AOE)
			animation = load("res://assets/PNJ/Animations/AnimatedSprite_F_Malade.tscn").instance()
			self.add_child(animation)
		"F malade":
			AOE.damage = 20
			self.add_child(AOE)
			animation = load("res://assets/PNJ/Animations/AnimatedSprite_F_Malade.tscn").instance()
			self.add_child(animation)
		"Homme":
			AOE.damage = 5
			self.add_child(AOE)
			animation = load("res://assets/PNJ/Animations/AnimatedSprite_F.tscn").instance()
			self.add_child(animation)
		"Femme":
			AOE.damage = 5
			self.add_child(AOE)
			animation = load("res://assets/PNJ/Animations/AnimatedSprite_F.tscn").instance()
			self.add_child(animation)
			
		_:
			animation = load("res://assets/PNJ/Animations/AnimatedSprite_F_Malade.tscn").instance()
			self.add_child(animation)
	linenode = find_node("Line2D")
	if linenode != null:
		self.path = linenode.points
	
func _process(_delta) -> void:
	if type == "Policier":
		SightCheck()
	

func _physics_process(delta):
	move_along_path(delta)

func move_along_path(delta) -> void:
	if path.size() <= 0:
		return
	velocity = (path[0]-position).normalized()* delta *speed
	collision = move_and_collide(velocity)
	#go_to(path[0])
	if collision:
		if collision.collider == player:
			emit_signal("playercontact",self)	
	
	if velocity.length() > 0:
		animation.play()
	else:
		animation.stop()
		
	if velocity.x > 0 and (abs(velocity.x) > abs(velocity.y) ):
		animation.animation = "right"
	elif velocity.x < 0 and (abs(velocity.x) > abs(velocity.y)):
		animation.animation = "left"
	elif velocity.y < 0:
		animation.animation = "up"
	elif velocity.y > 0:
		animation.animation = "down"
	else:
		animation.animation = "idle"
	
	
func set_path(value: PoolVector2Array) -> void:
	path = value
	if value.size() == 0:
		return


func SightCheck():
	if player_in_range == true:
		var space_state = get_world_2d().direct_space_state
		var sight_check = space_state.intersect_ray(position,player.position,[self], collision_mask)
		if sight_check:
			if sight_check.collider.name == "Player":
				player_in_sight = true
				_on_seen()
				emit_signal("playerseen")
			else:
				player_in_sight = false
	elif player_in_sight == false:
		pass
		#if (self.position != initposition):
		#	self.path = nav_2d.get_simple_path(initposition,self.position)
		#elif player_in_sight == false && linenode != null:
		#	if path.size == 0:
		#		self.path = linenode.points
	
		
	

func _on_Sight_body_entered(body):
	if body == player:
		player_in_range = true

func _on_Sight_body_exited(body):
	if body == player:
		player_in_range = false


func _on_seen():
	var PJpos = player.global_position
	var PNJ_path = nav_2d.get_simple_path(PJpos,self.global_position)
	self.path = PNJ_path

func set_type(v):
	type = v

func get_type():
	return type
	
func go_to(pos):
	position = pos

		
			
		

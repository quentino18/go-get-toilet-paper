extends Control

onready var sprint_bar = $TextureProgress

func _ready():
	sprint_bar.max_value = 1000
	sprint_bar.value = sprint_bar.max_value
	
func _on_sprint_updated(sprint):
	sprint_bar.value = sprint

func _on_max_sprint_updated(sprint):
	sprint_bar.max_value = sprint

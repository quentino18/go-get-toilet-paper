extends Control

onready var health_bar = $TextureProgress

func _ready():
	health_bar.max_value = 100
	health_bar.value = health_bar.max_value

func _on_health_updated(health):
	health_bar.value = health


func _on_max_health_updated(health):
	health_bar.max_value = health

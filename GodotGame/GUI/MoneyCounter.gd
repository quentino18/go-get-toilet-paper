extends Control

onready var money_display = $Background/Number
signal game_over(reason)

func _ready():
	self.connect("game_over",self.get_tree().get_root(),"_on_game_over")

func _on_money_updated(money):
	money_display.text= String(money)
	
func _on_money_loss(loss):
	money_display.text = String(int(money_display.text) - loss)
	if money_display.text == "0":
		emit_signal("game_over", "you've been put in jail for not paying the fee")

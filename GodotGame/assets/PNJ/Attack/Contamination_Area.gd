extends Area2D

onready var type = get_parent().get_type()
onready var player = get_tree().get_root().find_node("Player",true,false)

export var damage: int setget set_damage, get_damage

func _ready():
	if type == "H malade" || type == "F malade":
		self.damage = 20

func _process(delta):
	# get target in area of effect
	var targets = get_overlapping_bodies()
	for target in targets:
		if target.has_method("_on_Hit"):
			#hit the player
			target._on_Hit(damage)

func set_damage(d):
	damage = d
	
func get_damage():
	return damage
